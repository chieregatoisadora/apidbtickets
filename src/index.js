const express = require('express')
const app = express();
const cors = require('cors');

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
    }

    middlewares() {
      this.express.use(express.json());
      this.express.use(cors());
    }

    routes() {
      const apiRoutes= require('./routes/rotas');
      this.express.use('/api/sistema/', apiRoutes);
    }
  }

  module.exports = new AppController().express;