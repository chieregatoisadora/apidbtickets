const { json } = require("express");
const connect = require("../db/connect");

module.exports = {
  // Método para obter todos os nomes das tabelas
  async getTables(req, res) {
    const queryShowTables = "show tables";

    connect.query(queryShowTables, async function (err, result, fields) {
      if (err) {
        console.log(err);
        return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" });
      }

      const tableNames = result.map(row => row[fields[0].name]);
      console.log("Tabelas do banco de dados:", tableNames);

      res.status(200).json({ message: "Nomes das tabelas do banco de dados", tables: tableNames });
    });
  },

  // Método para obter todas as descrições das tabelas e seus atributos
  async getDescTable(req, res) {
    const queryShowTables = "show tables";

    connect.query(queryShowTables, async function (err, result, fields) {
      if (err) {
        console.log(err);
        return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" });
      }

      const tables = [];

      for (let i = 0; i < result.length; i++) {
        const tableName = result[i][`Tables_in_${connect.config.connectionConfig.database}`];
        const queryDescTable = `describe ${tableName}`;

        try {
          const tableDescription = await new Promise((resolve, reject) => {
            connect.query(queryDescTable, function (err, result, fields) {
              if (err) {
                reject(err);
              }
              resolve(result);
            });
          });
          tables.push({ name: tableName, description: tableDescription });
        } catch (error) {
          console.log(error);
          return res.status(500).json({ error: "Erro ao obter a descrição da tabela!" });
        }
      }

      res.status(200).json({ message: "Descrição das tabelas do banco de dados", tables });
    });
  }
};
